## videos
### Kubernetes
- https://www.youtube.com/watch?v=kOa_llowQ1c  Kubernetes The Easy Way! (For Developers In 2018)
- https://www.youtube.com/watch?v=XPC-hFL-4lU  GopherCon 2017: Kelsey Hightower - Self Deploying Kubernetes Applications
- https://www.youtube.com/watch?v=XL9CQobFB8I  Container management and deployment: from development to production (Google Cloud Next '17)
### batch processing
- https://www.youtube.com/watch?v=Zgqrd0XQwTw  AWS re:Invent 2017: Batch Processing with Containers on AWS (CON304)
